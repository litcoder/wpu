<?php
/**
 * Plugin Name: PlantUML plugin for WordPress
 * Plugin URI: https://bitbucket.org/litcoder/wpu
 * Description: This plugin allows you to draw UMLs with PlantUML grammar in WordPress. Describe your UMLs surrounded by [wpu] ~ [/wpu] short code. e.g) [wpu]Bob -> Alice:hello[/wpu]
 * Version: 1.0
 * Author: Litcoder
 * Author URI: http://www.litcoder.com
 * License: GPL2 or later
 */

require_once("WpuDefs.php");
require_once("WpuDebug.php");
require_once("WpuUMLWebCreator.php");
require_once("WpuUMLLocalCreator.php");


function wpu_to_image($umlsrc)
{
    // Local UML creator
    if (WPU_LOCAL_CREATOR) {
        $creator = new WpuUMLLocalCreator;
        $outFormat = '%s';
    } 
    // Web UML creator
    else {
        $creator = new WpuUMLWebCreator;
        $outFormat = '<img src=%s>';
    }

    $img = $creator->create($umlsrc);
    $imgTag = sprintf($outFormat, $creator->create($umlsrc));

    return $imgTag;
}


function wpu_draw_uml_in($attrs, $content)
{
    return wpu_to_image($content);
}

/*
 * HOOKs
 */

// Hook for the shortcode represnetation
add_shortcode('wpu', 'wpu_draw_uml_in');


// Add button to visual editor
add_filter('mce_buttons', 'wpu_add_button');
function wpu_add_button($buttons)
{
    array_push($buttons, 'wpu_edit_uml');
    return $buttons;
}

// Add plugin
add_filter('mce_external_plugins', 'wpu_add_tinymce_javascript');
function wpu_add_tinymce_javascript($plugin_array)
{
    $plugin_array['wpu'] = plugins_url( '/js/wpu_tinymce_plugin.js', __FILE__ ) ;
    return $plugin_array;
}
?>
