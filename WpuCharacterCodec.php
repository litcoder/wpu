<?php
/*
  FILE: WpuCharacterCodec.php
 *
 * To use PlantUML image generation, a text diagram description have to be :
 *
 *   - Encoded in UTF-8
 *   - Compressed using Deflate algorithm
 *   - Reencoded in ASCII using a transformation close to base64 
 */


require_once("WpuDefs.php");
require_once("WpuDebug.php");


class WpuCharacterCodec {

    private $htmlTrTable 
        = array('&quot;' => '"', '&amp' => '&', '&lt;' => '<', '&gt;' => '>', '&#8211;' => '--', '&#8212;' => '--');


    # Retrieves UML source back from the string
    public function retrieveUmlSource($tagMixedText) {
        $tagStripped = $this->stripTags($tagMixedText);
        $umlSource = strtr($tagStripped, $this->htmlTrTable);
        WpuDebug::info("Decoded UML source", $umlSource);

        return $umlSource;
    }

    // PHP's strip_tags() function has a bug that strips non tags bug starts with '<'.
    private function stripTags($tagMixedText) {
        // Starting tags : <\s*[a-z]+(\s+(=|[a-z]+|\'|\"|_)*)*>
        // Closing tags : <\/\s*[a-z]+>)
        // Self closing tags : <[a-z]+\s*\/>
        $s = preg_replace(
            "/(<\s*[a-z]+(\s+(=|[a-z]+|\'|\"|_)*)*>|<\/\s*[a-z]+>)|<[a-z]+\s*\/>/i"
            , ""
            , $tagMixedText);
        return $s;
    }

    public function encode($umlSource) {

        $utf8 = $this->convertToUTF8($umlSource);
        $compressed = gzdeflate($utf8, 9);
        $base64 = $this->encode64($compressed);

        # Deubgging information
        WpuDebug::info("umlSource", $umlSource);
        WpuDebug::printInBytes("UTF8\t\t", $utf8);
        WpuDebug::printInBytes("COMPRESSED\t", $compressed);
        WpuDebug::printInBytes("Base64\t\t", $base64);

        return $base64;
    }

    public function convertToUtf8($str) {
        $currentEncoding = mb_detect_encoding($str);
        $convStr = iconv($currentEncoding, 'UTF-8//IGNORE', $str);
        return $convStr;
    }

    public function stringToBytes($str) {
        $chars = array();
        for ($i = 0; $i < strlen($str); $i++ ) {
            $c = substr($str, $i, 1);
            $chars[] = ord($c);
        }
        return $chars;
    }


/**
 * Encoding functions
 * Functions in below were from PlantUML's following URL.
 *  http://plantuml.sourceforge.net/codephp.html
 * 
 **/
    private function encode6bit($b) { 
        if ($b < 10) { 
            return chr(48 + $b); 
        } 
        $b -= 10; 
        if ($b < 26) { 
            return chr(65 + $b); 
        } 
        $b -= 26; 
        if ($b < 26) { 
            return chr(97 + $b); 
        } 
        $b -= 26; 
        if ($b == 0) { 
            return '-'; 
        } 
        if ($b == 1) { 
            return '_'; 
        } 
        return '?'; 
    } 

    private function append3bytes($b1, $b2, $b3) { 
        $c1 = $b1 >> 2; 
        $c2 = (($b1 & 0x3) << 4) | ($b2 >> 4); 
        $c3 = (($b2 & 0xF) << 2) | ($b3 >> 6); 
        $c4 = $b3 & 0x3F; 
        $r = ""; 
        $r .= $this->encode6bit($c1 & 0x3F); 
        $r .= $this->encode6bit($c2 & 0x3F); 
        $r .= $this->encode6bit($c3 & 0x3F); 
        $r .= $this->encode6bit($c4 & 0x3F); 
        return $r; 
    } 

    private function encode64($c) { 
        $str = ""; 
        $len = strlen($c); 
        for ($i = 0; $i < $len; $i+=3) { 
            if ($i+2==$len) { 
                $str .= $this->append3bytes(ord(substr($c, $i, 1)), ord(substr($c, $i+1, 1)), 0); 
            } else if ($i+1==$len) { 
                $str .= $this->append3bytes(ord(substr($c, $i, 1)), 0, 0); 
            } else { 
                $str .= $this->append3bytes(ord(substr($c, $i, 1)), ord(substr($c, $i+1, 1)), ord(substr($c, $i+2, 1))); 
            } 
        } 
        return $str; 
    } 

}

?>