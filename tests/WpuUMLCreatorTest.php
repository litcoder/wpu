<?php


require_once("WpuUMLLocalCreator.php");
require_once("WpuUMLWebCreator.php");


class WpuUMLCreatorTest extends PHPUnit_Framework_TestCase {

    public function testAvailability() {
        if(WPU_LOCAL_CREATOR === FALSE) {
            // Assume the web is always avialable.
            $webCreator = new WpuUMLWebCreator;
            $available = $webCreator->isAvailable();
            $this->assertTrue($available);
        } else {
            // Check the local UML creator's availability.
            $localCreator = new WpuUMLLocalCreator;
            $available = $localCreator->isAvailable();
            $this->assertTrue($available, "Files for a local UML creation are not available.");
        }
    }

    public function testWebCreator() {
        if(WPU_LOCAL_CREATOR === FALSE) {
            return;
        }

        $testUML = "Alice -> Bob";
        $expectedWebCreatorOutput = "http://www.plantuml.com:80/plantuml/svg/Syp9J4vLqBLJSCfF0W00";

        $webCreator = new WpuUMLWebCreator;
        $umlResult = $webCreator->create($testUML);
        $this->assertEquals($expectedWebCreatorOutput, $umlResult);
    }

    public function testLocalCreator() {

        if(WPU_LOCAL_CREATOR === FALSE) {
            return;
        }

        $testUML = "Alice -> Bob";
        $expectedOutput = 
            '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="110pt" style="width:118px;height:110px;" version="1.1" viewBox="0 0 118 110" width="118pt"><defs><filter height="300%" id="f1" width="300%" x="-1" y="-1"><feGaussianBlur result="blurOut" stdDeviation="2.0"/><feColorMatrix in="blurOut" result="blurOut2" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .4 0"/><feOffset dx="4.0" dy="4.0" in="blurOut2" result="blurOut3"/><feBlend in="SourceGraphic" in2="blurOut3" mode="normal"/></filter></defs><g><line style="stroke: #A80036; stroke-width: 1.0; stroke-dasharray: 5.0,5.0;" x1="33" x2="33" y1="38.29" y2="72.29"/><line style="stroke: #A80036; stroke-width: 1.0; stroke-dasharray: 5.0,5.0;" x1="91" x2="91" y1="38.29" y2="72.29"/><rect fill="#FEFECE" filter="url(#f1)" height="30.29" style="stroke: #A80036; stroke-width: 1.5;" width="47" x="8" y="3"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="33" x="15" y="23.3027">Alice</text><rect fill="#FEFECE" filter="url(#f1)" height="30.29" style="stroke: #A80036; stroke-width: 1.5;" width="47" x="8" y="71.29"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="33" x="15" y="91.5928">Alice</text><rect fill="#FEFECE" filter="url(#f1)" height="30.29" style="stroke: #A80036; stroke-width: 1.5;" width="40" x="69" y="3"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="26" x="76" y="23.3027">Bob</text><rect fill="#FEFECE" filter="url(#f1)" height="30.29" style="stroke: #A80036; stroke-width: 1.5;" width="40" x="69" y="71.29"/><text fill="#000000" font-family="sans-serif" font-size="14" lengthAdjust="spacingAndGlyphs" textLength="26" x="76" y="91.5928">Bob</text><polygon fill="#A80036" points="79,50.29,89,54.29,79,58.29,83,54.29" style="stroke: #A80036; stroke-width: 1.0;"/><line style="stroke: #A80036; stroke-width: 1.0;" x1="33.5" x2="85" y1="54.29" y2="54.29"/></g></svg>';

        $umlCreator = new WpuUMLLocalCreator;
        $umlResult = $umlCreator->create($testUML);
        $this->assertEquals($expectedOutput, $umlResult);

    }

    public function testLocalCreatorErrorHandling() {

        if(WPU_LOCAL_CREATOR === FALSE) {
            return;
        }

        // When the UML string is null
        $testUML = null;
        $umlCreator = new WpuUMLLocalCreator;

        $umlResult = $umlCreator->create($testUML);
        $this->assertNull($umlResult);


        // When the UML string is not valid
        $testUML = "Alice -> Bob";
        $umlValid = $umlCreator->isValidGrammar($testUML);
        $this->assertTrue($umlValid);

        $testUML = "NOT VALID UML STRING";
        $umlValid = $umlCreator->isValidGrammar($testUML);
        $this->assertFalse($umlValid);
    }
}

?>