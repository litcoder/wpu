<?php

require_once("WpuDefs.php");
require_once("WpuDebug.php");

class WpuDebugTest extends PHPUnit_Framework_TestCase {

    private $LOREM_IPSUM_TEXT = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est labosdf";

    public function testWarn() {
        if(WPU_DEBUG) {
            $title = "Title";
            $note =  $this->LOREM_IPSUM_TEXT;

      
            ob_start();
            WpuDebug::warn($title, $note);
            $taggedOutput = ob_get_contents();
            ob_end_flush();

            $expected = 
                '<P><FONT color="'
                . WpuDebug::$COLOR_WARN
                . '"><B>' 
                . $title
                . ':</B> '
                . $note
                . '</FONT></P>';
            $this->assertContains($expected, $taggedOutput, '', true);
        }
    }
}
?>