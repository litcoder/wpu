<?php

require_once("WpuCharacterCodec.php");


class WpuCharacterCodecTest extends PHPUnit_Framework_TestCase {

    public function testEncoding() {
        $chcodec = new WpuCharacterCodec;

        $this->assertEquals("SrJGjLDm0W00", $chcodec->encode("A -> B"));
    }

    public function testRetrieving() {
        $chcodec = new WpuCharacterCodec;

        $testStr = "A -> B";
        $this->assertEquals("A -> B", $chcodec->retrieveUmlSource($testStr));

        $testStr = "A <- B";
        $this->assertEquals("A <- B", $chcodec->retrieveUmlSource($testStr)); 
        
        
/*
        $testStr = "<TAG>A -> B</TAG>";
        $this->assertEquals("A -> B", $chcodec->retrieveUmlSource($testStr));

        $testStr = "<TAG class='complex_class'>A -> B</TAG>";
        $this->assertEquals("A -> B", $chcodec->retrieveUmlSource($testStr));

        $testStr = "<TAG class='complex_class' id='test_id'>A -> B</TAG>";
        $this->assertEquals("A -> B", $chcodec->retrieveUmlSource($testStr));

        $testStr = "<TAG class='complex_class' id='test_id'>A -> B<br/><br  /></TAG>";
        $this->assertEquals("A -> B", $chcodec->retrieveUmlSource($testStr));
*/
    }


    public function testStringToBytes() {
        $chcodec = new WpuCharacterCodec;

        # int bytes for the string ABCabc
        $expectedArray = array(0x41, 0x42, 0x43, 0x61, 0x62, 0x63);
        $this->assertEquals($expectedArray, $chcodec->stringToBytes('ABCabc'), "ASCII strings must be same characters in byte array after the conversion");


        # Test unicode bytes transition  - http://en.wikipedia.org/wiki/UTF-8
        #
        # Korean
        # 안녕 - <%-U+C548-%><%-U+B155-%>
        # U+C548 : 1100 0101 0100 1000
        #    => 1110 1100 1001 0101 1000 1000 (0xEC, 0x95, 0x88)
        # U+B155 : 1011 0001 0101 0101
        #    => 1110 1011 1000 0101 1001 0101 (0xEB, 0x85, 0x95)
        $expectedArray = array(0xEC, 0x95, 0x88, 0xEB, 0x85, 0x95);
        $this->assertEquals($expectedArray, $chcodec->stringToBytes('안녕'));


        # Mixed of ASCII chars and unicode
        $expectedArray = array(0xEC, 0x95, 0x88, 0xEB, 0x85, 0x95, 0x41, 0x42, 0x43, 0x61, 0x62, 0x63);
        $this->assertEquals($expectedArray, $chcodec->stringToBytes('안녕ABCabc'));
    }


    public function testUTF8Conversion() {
        WpuDebug::info(PHP_EOL.PHP_EOL.'TESTCASE', 'testUTF8Conversion');
        $chcodec = new WpuCharacterCodec;


        $testString = 'A -> B';
        $utf8String = $chcodec->convertToUtf8($testString);
        $expectedArray = array(0x41, 0x20, 0x2d, 0x3e, 0x20, 0x42);
        $convArray = $chcodec->stringToBytes($utf8String);
        $this->assertEquals($expectedArray, $convArray);

/*
        # Converting to UTF8 from EUC-KR
        $testString = 'A -> B:안녕';
        $currentEncoding = mb_detect_encoding($testString);
        WpuDebug::info('Current encoding', $currentEncoding);
        $eucKrString =  iconv(
            $currentEncoding,
            'EUC-KR',
            $testString);

        $utf8String = $chcodec->convertToUtf8($eucKrString);
        $expectedArray = array(0x41, 0x20, 0x2d, 0x3e, 0x20, 0x42, 0x3a, 0xEC, 0x95, 0x88, 0xEB, 0x85, 0x95);

        $convArray = $chcodec->stringToBytes($utf8String);
        $this->assertEquals($expectedArray, $convArray);
*/
    }


    public function testBase64Encoding() {
        $chcodec = new WpuCharacterCodec;


        # HINT: You may can these expected answers from the PlantUML's online UML drawing web site.
        #       To get base64 encoded results, see the encoded strings at the end of the URL after the conversion.
        #   - http://www.plantuml.com/plantuml/form
        $testStr = "A -> B";
        $this->assertEquals('SrJGjLDm0W00', $chcodec->encode($testStr));

        $testStr = "A -> B:hello";
        $this->assertEquals('SrJGjLDmiif8pSd91m00', $chcodec->encode($testStr));

        $testStr = "A -> B:안녕";
        $this->assertEquals('SrJGjLDmidepjUDrwrG0', $chcodec->encode($testStr));
   }

}

?>