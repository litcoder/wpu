<?php
/*
  FILE: WpuDebug.php
*/

require_once("WpuDefs.php");

class WpuDebug {
    public static $COLOR_WARN = "#008800";
    public static $COLOR_ERROR = "#880000";
    public static $COLOR_INFO = "#000088";


    private static function printString($title, $note, $color){
        if(WPU_DEBUG) {
            if(WPU_LOG_HTML_TAGGED) {
                echo '<P><FONT color="'
                    . $color
                    . '"><B>' 
                    . $title
                    . ':</B> '
                    . $note
                    . '</FONT></P>';
            } else {
                echo $title . ': ' . $note . PHP_EOL;
            }
        }
    }


    public static function warn($title, $note) {
        self::printString($title, $note, self::$COLOR_WARN);
    }

    public static function error($title, $note) {
        self::printString($title, $note, self::$COLOR_ERROR);
    }

    public static function info($title, $note) {
        self::printString($title, $note, self::$COLOR_INFO);
    }

    public static function printInBytes($title, $str) {

        if(WPU_DEBUG) {
            echo $title . ": ";
            for ($i = 0; $i < mb_strlen($str); $i++ ) {
                $c = mb_substr($str, $i, 1);
                printf('0x%x ', ord($c));
            }

            echo PHP_EOL;
        }
    }
}


?>