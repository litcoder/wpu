<?php

require_once("WpuDefs.php");
require_once("WpuDebug.php");
require_once("WpuCharacterCodec.php");


abstract class WpuUMLCreator {

    protected $mChCodec = null;


    public function __construct() {
        $this->init();
        $this->mChCodec = new WpuCharacterCodec;
    }

    abstract public function init();
    abstract public function create($umlSource);
    abstract public function isAvailable();

    public function retrieveUmlSource($tagMixedText) {
        if ($this->mChCodec != null) {
            return $this->mChCodec->retrieveUmlSource($tagMixedText);
        }

        WpuDebug::error("Init fail", "Character Codes has not been initialized!");
        return null;
    }
}

?>