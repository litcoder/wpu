.SUFFIXES: .js .min.js

# Name of the releasing package.
PACKAGE_NAME = wpu_

# A place where all outputs for releasing exists.
OUTPUT_DIR = wpu

# Javascript files.
OUTPUT_JS = \
	$(OUTPUT_DIR)/js/plantuml.js\
	$(OUTPUT_DIR)/js/rawdeflate.js\
	$(OUTPUT_DIR)/js/rawinflate.js\
	$(OUTPUT_DIR)/js/wpu_previewuml.jS\
	$(OUTPUT_DIR)/js/wpu_tinymce_plugin.js

# Minfied Javascript fiels.
OUTPUT_JS_MIN = \
	$(OUTPUT_DIR)/js/plantuml.min.js \
	$(OUTPUT_DIR)/js/rawdeflate.min.js \
	$(OUTPUT_DIR)/js/wpu_previewuml.min.js \
	$(OUTPUT_DIR)/js/wpu_tinymce_plugin.min.js

# PHP files except unittest files.
OUTPUT_PHP = \
	$(OUTPUT_DIR)/WpuCharacterCodec.php \
	$(OUTPUT_DIR)/WpuDebug.php \
	$(OUTPUT_DIR)/WpuDefs.php \
	$(OUTPUT_DIR)/WpuMain.php \
	$(OUTPUT_DIR)/WpuUMLCreator.php \
	$(OUTPUT_DIR)/WpuUMLLocalCreator.php \
	$(OUTPUT_DIR)/WpuUMLWebCreator.php

# HTML files.
OUTPUT_HTML = \
	$(OUTPUT_DIR)/EditDialog.html


# Minifier options
# We're using yuicompressor http://yui.github.io/yuicompressor
JAVAVM = java
JAVAVM_ARGS = -jar
JS_MINIFIER = bin/yuicompressor.jar

# A new suffix rules to make minified Javascript using YUI compressor.
.js.min.js:
	@$(JAVAVM) $(JAVAVM_ARGS) $(JS_MINIFIER) $< -o $*.min.js


release: zip_output_files

create_output_dir:
	@mkdir $(OUTPUT_DIR)

copy_output: create_output_dir
	@echo "Copying files..."
	@find \( \( -path './.git' -prune \) -or \( -path './tests' -prune \) -or \( -path './bin' -prune \) \) -or \( -type f -and -not -name '.git*' \) -print | cpio -pdm $(OUTPUT_DIR)

minify_js: $(OUTPUT_JS_MIN)
	@echo "Minifying Javascripts..."

ref_min_js: $(OUTPUT_PHP) $(OUTPUT_HTML)
	@sed -i 's/\.js/\.min\.js/g' $?

# Delete origin javascripts from the OUTPUT_DIR before zipping.
del_org_js: $(OUTPUT_JS)
	@rm $?

zip_output_files: copy_output minify_js ref_min_js del_org_js
	@echo "Zipping $(OUTPUT_DIR)..."
	@zip -r $(PACKAGE_NAME).zip $(OUTPUT_DIR)

clean:
	@rm -rf $(OUTPUT_DIR)
	@rm -rf $(PACKAGE_NAME).zip


