function previewUml(umlSrc) {
    var plantUMLImgAddr="http://www.plantuml.com/plantuml/img/";
    umlSrc = unescape(encodeURIComponent(umlSrc));
    var encodedSrc = encode64(deflate(umlSrc, 9));

    if(encodedSrc != ""){
        $('umlOutputimg').src=plantUMLImgAddr + encodedSrc;
    } else {
        $('umlOutputimg').src="";
    }
}