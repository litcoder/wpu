var PLUGIN_NAME = 'WPU editor';
var PLUGIN_TITLE = 'WPU UML editor';
var AUTHOR = 'Litcoder';
var AUTHOR_URL = 'http://www.litcoder.com';
var PLUGIN_INFO_URL = 'http://www.litcoder.com/?p=1082';
var VERSION = '1.0';

var DIALOG_WIDTH = 600;
var DIALOG_HEIGHT = 500;


(function() {
    tinymce.create('tinymce.plugins.Wpu', {
            init : function(ed, url) {
            ed.addButton( 'wpu_edit_uml', {
                title : PLUGIN_TITLE,
                image : url + '/../img/wpuml.jpg',
                // Open a new window for UML input.
                onclick: function() {
                    ed.windowManager.open({
                        file: url + '/../EditDialog.html',
                        title: PLUGIN_NAME,
                        width: DIALOG_WIDTH,
                        height: DIALOG_HEIGHT,
                        buttons: [
                                  {text: 'Insert', onclick: function() {
                                      var win = ed.windowManager.getWindows()[0];
                                      var insertData = win.getContentWindow().document.getElementById('umlCode').value;
                                      insertData = insertData.replace(/(?:\r\n|\r|\n)/g, '<br/>');
                                      ed.insertContent("[wpu]<br/>" + insertData + "<br/>[/wpu]");
                                      win.close();
                                  }},
                                  {text: 'Close', onclick: 'close'}]
                    });
                }
            });
        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : PLUGIN_NAME,
                author : AUTHOR,
                authorurl : AUTHOR_URL,
                infourl : PLUGIN_INFO_URL,
                version : VERSION
            };
        }
    });
    tinymce.PluginManager.add( 'wpu', tinymce.plugins.Wpu );
})();
