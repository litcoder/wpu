<?php

require_once("WpuUMLCreator.php");


class WpuUMLWebCreator extends WpuUMLCreator {

    private $mServer = "http://www.plantuml.com:80/plantuml/";
    private $mFormat = "svg";
    private $mURL = null;


    public function init() {
        if($this->mURL == null) {
            $this->mURL = $this->mServer . $this->mFormat . "/";
        }
    }

    public function create($tagMixedText) {
        $umlSource = $this->retrieveUmlSource($tagMixedText); 
        $e = $this->encode($umlSource);

        return $this->mURL . $e;
    }

    public function isAvailable() {
        // Assuem the web creator is available.
        return TRUE;
    }

    public function encode($umlSource) {
        return $this->mChCodec->encode($umlSource);
    }
}

?>