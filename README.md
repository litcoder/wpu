# README #
This README would normally document whatever steps are necessary to get your application up and running.


### What is the WPU? ###
WPU is a WordPress plugin which allows you to draw UMLs using [PlantUML](http://plantuml.sourceforge.net) grammar.


### How do I get set up and run? ###
Download the zip file and install it from the WordPress's plugin management page. 


####Versions after 1.0####
Once the installation has been completed (and activated), you can draw UMLs by surrounding PlantUML defined UML descriptions between [wpu]~[/wpu] short code.

e.g)

```
#!php

[wpu]
Alice -> Bob: hi
Bob --> Alice: hello
[/wpu]
```

####Old versions before 0.5 (Deprecated)####
Once the installation has been completed (and activated), you can draw UMLs by surrounding PlantUML defined UML descriptions between 'div' tags with 'plantuml' class.

e.g)

```
#!php

<div class="plantuml">
Alice -> Bob: hi
Bob --> Alice: hello
</div>
```



### Running WPU independently ###
WPU's default configuration assumes a situation that you are not free to install software packages on your WordPress running servers. So WPU sends your UML description to the server which is being provided by PlantUML and gets image link from it by default.
However, if following software packages are supported by your server environment or installable, you can configure not to send your UML descriptions to the PlantUML server.

* Java : Tested with Oracle JDK 1.6.0_45.
* Plantuml.jar : Get it from the PlantUML's download page.(http://plantuml.com/download.html)
* Graphviz : On ubuntu, you can install it with 'sudo apt-get install graphviz' command.

When all these softwares are available, edit WpuDefs.php to run local UML creation.


```
#!php

<?php
...
define('WPU_LOCAL_CREATOR', TRUE);   // Change this from FALSE to TRUE
define('WPU_LOCAL_CREATOR_JAVA_PATH', '/ABSOLUTE/PATH/TO/java');   // Set to absolute path to the java binary
define('WPU_LOCAL_CREATOR_PLANTUML_PATH', '/ABSOLUTE/PATH/TO/plantuml.jar');  // Set to absolute path to plantuml.jar file
?>
```


### External dependency of this plugin ###
* rawdeflate.js and rawinflate.js are taken from https://github.com/johan/js-deflate
* plantuml.js is from http://plantuml.com/codejavascript.html
* YUI compressor is from http://yui.github.io/yuicompressor/
* plantuml.jar is from http://plantuml.com/download.html



### Who do I talk to? ###

* Twitter: @ litcoder