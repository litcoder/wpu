<?php

# Print define information
define('WPU_DEBUG', FALSE);


# HTML tagged output
# Set to FALSE to get plane text debug output.
define('WPU_LOG_HTML_TAGGED', TRUE);


# [WPU UML LOCAL CREATION]
#    Does local creation available? Use Web creation if not.
define('WPU_LOCAL_CREATOR', FALSE);
define('WPU_LOCAL_CREATOR_JAVA_PATH', '/ABSOLUTE/PATH/TO/java');
define('WPU_LOCAL_CREATOR_PLANTUML_PATH', '/ABSOLUTE/PATH/TO/plantuml.jar');

?>
