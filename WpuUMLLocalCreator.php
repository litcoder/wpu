<?php

require_once("WpuDefs.php");
require_once("WpuUMLCreator.php");


class WpuUMLLocalCreator extends WpuUMLCreator {

    private $mCommand = null;   /* UML creating command */
    private $mVCommand = null;  /* Command to check UML validity */
    private $mJavaPath = null;
    private $mPlantUMLJarPath = null;
    private $mFormat = "svg";


    private function runLocalCommand($cmd, $umlFile) {
        static $readSize = 8196;

        $output = '';

        // PHP will pass the descriptors to child.
        // 0 is stdin, 1 is stdout and 2 is stderr.
        $proc = proc_open(
            $cmd,
            array(0=>$umlFile, 1=>array('pipe', 'w')),   // 0 is stdin, 1 is stdout
            $pipes, null, null);

        if (is_resource($proc)) {

            while (true) {
                $data = fread($pipes[1], $readSize);
                if (strlen($data) == 0) {
                    break;
                }
                $output .= $data;
            }

            fclose($pipes[1]);
            proc_close($proc);
        }

        return $output;
    }


    public function init() {
        $this->mJavaPath = WPU_LOCAL_CREATOR_JAVA_PATH;
        $this->mPlantUMLJarPath = WPU_LOCAL_CREATOR_PLANTUML_PATH;

        $this->mCommand = 
            $this->mJavaPath 
            . " -jar " . $this->mPlantUMLJarPath 
            . " -charset UTF-8 -p -t" . $this->mFormat;

        $this->mVCommand = 
            $this->mJavaPath 
            . " -jar " . $this->mPlantUMLJarPath 
            . " -syntax -charset UTF-8 -p";
    }


    public function create($tagMixedText) {
        $img = '';
        $f = tmpfile();

        if(!is_resource($f) || $tagMixedText == null) {
            return null;
        }

        $umlSource = $this->retrieveUmlSource($tagMixedText);

        // Write UML source into the temp file and run a new process with it.
        fwrite($f, $umlSource);
        fseek($f, 0);

        $img = $this->runLocalCommand($this->mCommand, $f);
        fclose($f);

        return $img;
    }


    public function isAvailable() {
        /*
          To run local UML creator you need,
            1) Graphviz pagckage has installed on your machine.
            2) Path to the available java
            3) Path to the available plantuml.jar
         */
        if(
            is_readable(WPU_LOCAL_CREATOR_JAVA_PATH)
            && is_readable(WPU_LOCAL_CREATOR_PLANTUML_PATH)) {
            return TRUE;
        }
        return FALSE;
    }


    public function isValidGrammar($umlSource) {
        $f = tmpfile();

        if(!is_resource($f) || $umlSource == null) {
            return FALSE;
        }

        // Write UML source into the temp file and run a new process with it.
        fwrite($f, $umlSource);
        fseek($f, 0);

        $grmResult = $this->runLocalCommand($this->mVCommand, $f);
        fclose($f);

      // ERROR string will be shwon at the start if it's invalid UML string.
      $pos = strpos($grmResult, "ERROR");
      if($pos === 0) {
          return FALSE;
      }

      return TRUE;
    }
}

?>